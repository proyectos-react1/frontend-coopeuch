export const getAlumnos = async() => {

    const url = `http://localhost:9060/api/v1/getListaAlumnos`
    const resp = await fetch( url );
    const data = await resp.json();

    const listaAlumnos = data.map( lista => ({
        identificador: lista.identificador,
        descripcion: lista.descripcion,
        fechaCreacion: lista.fechaCreacion,
        vigente: lista.vigente
    }));

    return listaAlumnos;
}