import { useEffect, useState } from "react";
import { getAlumnos } from "./helpers/getListaAlumnos"

export const App = () => {

  const [alumnos, setAlumnos] = useState([]);

  const listaAlumnos = async() => {
    
    const newAlumnos = await getAlumnos();
    setAlumnos(newAlumnos);
  }

  useEffect( () => {
    listaAlumnos();
  }, [])


  return (
    <>
    <h1>Lista de Alumnos</h1>

    <table>
      <thead>
        <tr>
          <th>Identificador</th>
          <th>Descripción</th>
          <th>Fecha de Ingreso</th>
          <th>Vigente</th>
        </tr>        
      </thead>
      <tbody>
          { alumnos.map(({ id, identificador, descripcion, fechaCreacion, vigente }) => {
          return (
              <tr key={ id }>
                <td>{ identificador }</td>
                <td>{ descripcion }</td>
                <td>{ fechaCreacion }</td>
                <td>{ vigente }</td>
              </tr>
            );
          })}
      </tbody>
    </table>
    </>
  )
}
